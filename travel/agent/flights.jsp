                <section class="content-header">
                    <h1>
                        Travel Agent
                        <small>Flights Book</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Flights Book</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- START CUSTOM TABS -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab"><strong>One Way</strong></a></li>
                                    <li><a href="#tab_2" data-toggle="tab"><strong>Round Trip</strong></a></li>
                                    <li><a href="#tab_3" data-toggle="tab"><strong>Multi City</strong></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1" style="height : 400px">
                                    	<div>
                                    		<div>
		                                        <div class="form-group">
		                                            <label for="oneway_from">From</label>
		                                            <input type="text" class="form-control" id="oneway_from" placeholder="From">
		                                        </div>                                    		</div>
                                    		<div>
		                                        <div class="form-group">
		                                            <label for="oneway_to">To</label>
		                                            <input type="text" class="form-control" id="oneway_to" placeholder="To">
		                                        </div>
                                    		</div>
                                    	</div>
                                    	<div>
                                    		<div>
                                    			ccc
                                    		</div>
                                    		<div>
                                    			ddd
                                    		</div>
                                    	</div>
                                    	
										<!-- 
										<table align="left" border="0" cellpadding="1" cellspacing="1" style="width:500px">
											<tbody>
												<tr>
													<td>

													</td>
													<td>
														-
													</td>													
													<td>
													</td>
												</tr>
												<tr>
													<td>
														<!-- Date range -->
					                                    <div class="form-group">
					                                        <label>Date range:</label>
					                                        <div class="input-group">
					                                            <div class="input-group-addon">
					                                                <i class="fa fa-calendar"></i>
					                                            </div>
					                                            <input type="text" class="form-control pull-right" id="oneway_from_date"/>
					                                        </div><!-- /.input group -->
					                                    </div><!-- /.form group -->
													</td>
													<td>&nbsp;</td>
													<td>
														<!-- Date range -->
					                                    <div class="form-group">
					                                        <label>Date range:</label>
					                                        <div class="input-group">
					                                            <div class="input-group-addon">
					                                                <i class="fa fa-calendar"></i>
					                                            </div>
					                                            <input type="text" class="form-control pull-right" id="reservation"/>
					                                        </div><!-- /.input group -->
					                                    </div><!-- /.form group -->
													</td>
												</tr>
											</tbody>
										</table>
										 -->
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        The European languages are members of the same family. Their separate existence is a myth.
                                        For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                                        in their grammar, their pronunciation and their most common words. Everyone realizes why a
                                        new common language would be desirable: one could refuse to pay expensive translators. To
                                        achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                                        words. If several languages coalesce, the grammar of the resulting language is more simple
                                        and regular than that of the individual languages.
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                        This tab 3
                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->


                    </div> <!-- /.row -->
                    <!-- END CUSTOM TABS -->

                </section><!-- /.content -->

<script type="text/javascript">
	//Date range picker
	$('#oneway_from_date').daterangepicker();
	$('#reservation').daterangepicker();
</script>
